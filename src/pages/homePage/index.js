import React from 'react';
import Header from "../../components/header";
import Footer from "../../components/footer";
import HowToPlayBlock from "../../components/howToPlayBlock";
import HowGetWinBlock from "../../components/howGetWinBlock";
import WelcomeBlock from "../../components/welcomeBlock";
import FormBlock from "../../components/formBlock";

const HomePage = () => {
    return (
        <div>
            <Header/>
            <WelcomeBlock/>
            <HowToPlayBlock/>
            <HowGetWinBlock/>
            <FormBlock/>
            <Footer/>
        </div>
    );
};

export default HomePage;
