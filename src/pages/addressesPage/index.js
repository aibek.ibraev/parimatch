import Header from '../../components/header';
import Adrresses from "../../components/addresses";
import Footer from "../../components/footer";

const AdrressesPage = () => (
    <>
        <Header />
        <Adrresses />
        <Footer />
    </>
);

export default AdrressesPage;
