import React from 'react';
import styles from './style.module.scss'

const WelcomeBlock = () => (
    <section className={styles.welcome}>
        <div>
            <div className='container'>
                <div className={styles.welcome_header}>
                    <div>
                        <h1>Лотерея<br/>&#171;ВГАДАЙ СЛОВО&#187;</h1>
                        <p>це шанс миттєво виграти солідний приз всього за 5 гривень!</p>
                    </div>
                </div>
            </div>
        </div>
        <div className={styles.radius_top}>
            <div className={styles.radius_half}></div>
        </div>
    </section>
);

export default WelcomeBlock;
