import React from 'react';
import styles from './style.module.scss'
import menuIcon from '../../assets/images/menuIcon.svg'
import {Link} from 'react-scroll'
import {Link as RouterLink} from 'react-router-dom'

const Header = () => {
    const [openList, setOpenList] = React.useState(false)
    return (
        <div className={styles.header}>
            <div className="container">
                <nav className={styles.nav}>
                    <div className={styles.logo}>
                        <RouterLink to='/'>
                            ВГАДАЙ СЛОВО
                        </RouterLink>
                    </div>
                    <ul className={styles.list}>
                        <li>
                            <Link to="howToPlayBlock" smooth={true} duration={500} offset={-70}>
                                ЯК ГРАТИ?
                            </Link>
                        </li>
                        <li>
                            <Link to="howGetWinBlock" smooth={true} duration={500} offset={-70}>
                                ЯК ОТРИМАТИ ВИГРАШ?
                            </Link>
                        </li>
                        <li>
                            <RouterLink to={'/addresses'}>
                                НАШI АДРЕСИ
                            </RouterLink>
                        </li>
                    </ul>
                    <div
                        className={styles.menu}
                        onClick={() => setOpenList(!openList)}
                    >
                        <img src={menuIcon} alt="menu"/>
                    </div>
                </nav>
            </div>
            {
                openList &&
                <div className={styles.list_mobile_wrapper}>
                    <div className="container">
                        <ul className={styles.list_mobile}>
                            <li>
                                <Link to="howToPlayBlock" smooth={true} duration={500} offset={0}>
                                    ЯК ГРАТИ?
                                </Link>
                            </li>
                            <li>
                                <Link to="howGetWinBlock" smooth={true} duration={500} offset={0}>
                                    ЯК ОТРИМАТИ ВИГРАШ?
                                </Link>
                            </li>
                            <li>
                                <RouterLink to={'/addresses'}>
                                    НАШI АДРЕСИ
                                </RouterLink>
                            </li>
                        </ul>
                    </div>
                </div>
            }
        </div>
    );
};

export default Header;
