import React from 'react';
import styles from './style.module.scss';
import {useRecoilState} from "recoil";
import {modalsAtom} from "../modals/state";
import {handleChangeModal} from "../../helpers/handleModalFunc";

const FormBlock = () => {
    const [modal, setOpenModal] = useRecoilState(modalsAtom)
    const [total , setTotal] = React.useState(5)
    return (
        <div className={styles.formBlock}>
            <div className={styles.formBlock_inner}>
                <div className="container">
                    <div className={styles.form_wrapper}>
                        <form className={styles.form}>
                            <div className={styles.top_block}>
                                <div className={styles.left}>
                                    <label htmlFor="nums">Номер тиражу:</label>
                                    <select name="nums" id="nums">
                                        <option value="1052">1052</option>
                                        <option value="1053">1053</option>
                                    </select>
                                </div>
                                <div className={styles.right}>
                                    <label htmlFor="bilet">Кількість білетів</label>
                                    <select name="bilet" id="bilet" onChange={e => setTotal(e.target.value * 5)}>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </select>
                                </div>
                            </div>
                            <div className={styles.bottom_block}>
                                <p>Загальна сумма: {total}.00 грн.</p>
                                <button
                                    className={'button'}
                                    onClick={(e) => handleChangeModal(e, 'login',setOpenModal)}
                                >КУПИТИ БIЛЕТ</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FormBlock;