import React from 'react';
import styles from './style.module.scss';
import {useRecoilState} from "recoil";
import {modalsAtom} from "../state";
import {handleChangeModal} from "../../../helpers/handleModalFunc";

const RegisterModal = () => {
    const [modal,setOpenModal] = useRecoilState(modalsAtom)
    return (
        <>
            {
                modal.current === 'register'
                &&
                <div className={styles.register}>
                    <form action="#">
                        <div className={styles.register_input}>
                            <label htmlFor="">E-mail <small>*</small></label>
                            <input
                                type="text"
                                className={'input'}
                            />
                            <span>
                                (Введіть вашу актуальну поштову скриньку)
                                Заборонені ресурси: mail.ru, yandex.ru, list.ru,
                                mail.ua, yandex.ua, inbox.ru, bk.ru, vk.ru, ok.ru,ya.ru
                            </span>
                        </div>
                        <div className={styles.register_input}>
                            <label htmlFor="">Телефон <small>*</small></label>
                            <input
                                type="text"
                                className={'input'}
                            />
                        </div>
                        <div className={styles.register_input_checkbox}>
                            <input type="checkbox" id="scales" name="scales"/>
                            <label htmlFor="scales">Згоден(а) з умовами Оферти</label>
                        </div>
                        <div className={styles.register_doc}>
                            <span
                                onClick={(e) => handleChangeModal(e, 'offer',setOpenModal)}
                            >
                                Документ Оферти
                            </span>
                        </div>
                        <div className={styles.register_input_checkbox}>
                            <input type="checkbox" id="scales" name="scales"/>
                            <label htmlFor="scales">Згоден(а) на обробку персональних данних </label>
                        </div>
                        <div className={styles.register_doc}>
                            <span
                                onClick={(e) => handleChangeModal(e, 'personal_data',setOpenModal)}
                            >
                                Документ Персональні дані
                            </span>
                        </div>
                        <div className={styles.register_input_checkbox}>
                            <input type="checkbox" id="scales" name="scales"/>
                            <label htmlFor="scales">
                                Я не  мешкаю на території АР
                                Крим та тимчасово окупованих територіях
                                Луганскої та Донецької областей
                            </label>
                        </div>
                        <button className={'button'}>
                            ДАЛI
                        </button>
                    </form>
                </div>
            }
        </>
    );
};

export default RegisterModal;