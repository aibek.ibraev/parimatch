import React from 'react';
import styles from './style.module.scss';
import {useRecoilState} from "recoil";
import {modalsAtom} from "../state";

const ResetLoginModal = () => {
    const [modal] = useRecoilState(modalsAtom)
    return (
        <>
            {
                modal.current === 'reset_login'
                &&
                <div className={styles.resetLogin}>
                    <h2>Відновлення логіну</h2>
                    <form action="#">
                        <input
                            type="text"
                            placeholder={'E-mail'}
                            className={'input'}
                        />
                        <button className={'button'}>
                            ВІДПРАВИТИ
                        </button>
                    </form>
                </div>
            }
        </>
    );
};

export default ResetLoginModal;