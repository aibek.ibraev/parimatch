import {atom} from "recoil";

export const modalsAtom = atom({
    key: "modals",
    default: {
        current: "",
    }
})

