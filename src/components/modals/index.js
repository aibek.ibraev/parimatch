import React from 'react';
import LoginModal from "./LoginModal";
import RegisterModal from "./RegisterModal";
import {useRecoilState} from "recoil";
import {modalsAtom} from "./state";
import {handleChangeModal} from "../../helpers/handleModalFunc";
import ResetLoginModal from "./ResetLoginModal";
import ResetPassModal from "./ResetPassModal";
import PersonDataModal from "./PersonalDataModal";
import OfferModal from "./OfferModal";

const Modals = () => {
    const [modal, setOpenModal] = useRecoilState(modalsAtom)
    return (
        <>
            {
                modal.current &&
                <div
                    className={'modal'}
                    onClick={(e) => handleChangeModal(e, '', setOpenModal)}>
                    <div
                        className={'modalContent'}
                        onClick={(e) => e.stopPropagation()}
                    >
                        <div
                            className={'times'}
                            onClick={(e) => handleChangeModal(e, '', setOpenModal)}>
                        ×</div>
                        <LoginModal/>
                        <RegisterModal/>
                        <ResetLoginModal/>
                        <ResetPassModal/>
                        <PersonDataModal/>
                        <OfferModal/>
                    </div>
                </div>
            }
        </>
    );
};

export default Modals;