import React from 'react';
import styles from './style.module.scss';
import {useRecoilState} from "recoil";
import {modalsAtom} from "../state";

const ResetPassModal = () => {
    const [modal] = useRecoilState(modalsAtom)
    return (
        <>
            {
                modal.current === 'reset_password'
                &&
                <div className={styles.resetPassword}>
                    <h2>Відновлення паролю</h2>
                    <form action="#">
                        <input
                            type="text"
                            placeholder={'Логiн або E-mail'}
                            className={'input'}
                        />
                        <button className={'button'}>
                            ВІДПРАВИТИ
                        </button>
                    </form>
                </div>
            }
        </>
    );
};

export default ResetPassModal;