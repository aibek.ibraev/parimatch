import React from 'react';
import styles from './style.module.scss';
import {useRecoilState} from "recoil";
import {modalsAtom} from "../state";
import {handleChangeModal} from "../../../helpers/handleModalFunc";

const LoginModal = () => {
    const [modal,setOpenModal] = useRecoilState(modalsAtom)
    return (
        <>
            {
                modal.current === 'login'
                &&
                <div className={styles.login}>
                    <h2>Вхiд в особистий кабiнет</h2>
                    <form action="#">
                        <input
                            type="text"
                            placeholder={'Логiн або E-mail'}
                            className={'input'}
                        />
                        <input
                            type="text"
                            placeholder={'Пароль'}
                            className={'input'}
                        />
                        <div className={styles.ant_captcha}>
                            <span>[anr-captcha]</span>
                        </div>
                        <button className={'button'}>
                            УВIЙТИ
                        </button>
                    </form>
                    <div className={styles.top_block}>
                        <span onClick={(e) => handleChangeModal(e, 'reset_password',setOpenModal)}>
                            Вiдновити пароль?
                        </span>
                        <span onClick={(e) => handleChangeModal(e, 'reset_login',setOpenModal)}>
                            Нагадати логiн
                        </span>
                    </div>
                    <div className={styles.bottom_block}>
                        <span>Ще немае аккаунта?</span>
                        <span
                            className={styles.register}
                            onClick={(e) => handleChangeModal(e, 'register',setOpenModal)}
                        >ЗЕРЕЄСТРУВАТИСЯ</span>
                    </div>
                </div>
            }
        </>
    );
};

export default LoginModal;