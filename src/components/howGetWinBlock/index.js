import React from 'react';
import styles from "./style.module.scss";

const HowGetWinBlock = () => (
    <section className={styles.how_get_win} id='howGetWinBlock'>
        <div className='container'>
            <h2>Як отримати виграш?</h2>
            <p className="elementor-text-editor elementor-clearfix">
                Виграші до 1 000 грн. включно виплачуються у будь-якому пункті розповсюдження лотерей (за наявності
                необхідної суми).
                Лотерейні квитки з виграшем 10 000 грн. і 50 000 грн. приймаються до оформлення окремо визначеними
                розповсюджувачами. Перелік розповсюджувачів розміщений на сайті у роздiлi
                <strong><a href="https://www.parimatch-lotto.com.ua/adress/">
                    <span> “Нашi адреси”</span>
                </a></strong>
            </p>
            <div className={styles.help}>
                <p>СЛУЖБА ПІДТРИМКИ&nbsp;<span>24/7</span></p>
                <p><span >Info_lottereya_ua@yahoo.com</span></p>
            </div>
        </div>
    </section>
);

export default HowGetWinBlock;
