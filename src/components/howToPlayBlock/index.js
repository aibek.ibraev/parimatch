import React from 'react';
import styles from './style.module.scss';
import lottery from '../../assets/images/loto111.png';

const HowToPlayBlock = () => (
    <section className={styles.how_play} id='howToPlayBlock'>
        <div className='container'>
            <h2>Як грати?</h2>
            <div className={styles.descriptions}>
                <div className={styles.text}>
                    <p>На кожному лотерейному квитку розміщені два контрольних слова і умови виграшу.</p>
                    <p>Кожна умова виграшу складається з букви і її порядкового номеру у слові. Кожна умова включає суму
                        можливого виграшу. Якщо будь-яка з умов збігається з будь-яким
                        контрольним словом — Ви виграли відповідну суму.</p>
                    <p>Припустимо, Ви купили такий лотерейний квиток. На ньому надруковані два слова:</p>
                    <p><span><strong>«ЗОЛОТО»</strong></span> і <span><strong>«ГОНОРАР»</strong></span>. Перевіряємо
                        умови виграшу: </p>
                    <ul>
                        <li><span>1 буква зліва «Д» — 20 грн.</span> — умова не
                            виконується, так у словах «ЗОЛОТО» і «ГОНОРАР» перші — інші літери.
                        </li>
                    </ul>
                    <ul>
                        <li><span>3 буква зліва «Е» — 50 000 грн</span>. — умова не виконується.</li>
                        <li><span>3 буква справу «О» — 50 грн.</span> — Ура! Виграш,
                            так у слові «ЗОЛОТО» третя буква справа О. Отже, квиток виграшний, сума виграшу — 50 гривень
                        </li>
                    </ul>
                </div>
                <img src={lottery} alt='lottery'/>
            </div>
        </div>
    </section>
);

export default HowToPlayBlock;
