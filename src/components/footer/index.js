import React from 'react';
import styles from './style.module.scss';
import Icon from '../../assets/images/18.png';
import OfertaPdf from '../../assets/pdf/oferta.pdf';
import PersPdf from '../../assets/pdf/pers.pdf';
import KonfPdf from '../../assets/pdf/konf.pdf';

const Footer = () => {
    return (
        <div className={styles.footer}>
            <div className="container">
                <div className={styles.top_block}>
                    <a href={OfertaPdf} target='_blank'>Загальна оферта</a>
                    <a href={PersPdf} target='_blank'>Полiтика конфiденцiйностi</a>
                    <a href={KonfPdf} target='_blank'>3года-повiдомлення</a>
                </div>
                <div className={styles.bottom_block}>
                    <div className={styles.left}>
                        <span>© 2021 «Вгадай слово» Всі права захищені (с).</span>
                    </div>
                    <div className={styles.right}>
                        <img src={Icon} alt="18+"/>
                        <p>Гравцем Лотереї може бути будь-яка право- та дієздатна особа, яка досягла 18-річного віку</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;