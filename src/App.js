import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import ScrollToTop from "./components/scrollToTop";
import HomePage from "./pages/homePage";
import AddressesPage from "./pages/addressesPage";

import './styles/global.scss';
import Modals from "./components/modals";

function App() {

    return (
      <Router>
          <Switch>
              <Route component={HomePage} path='/' exact/>
              <Route component={AddressesPage} path='/addresses' exact/>
          </Switch>
          <ScrollToTop/>
          <Modals/>
      </Router>
  );
}

export default App;
