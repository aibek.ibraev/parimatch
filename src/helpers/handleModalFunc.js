
export const handleChangeModal = (e, modal,func) => {
    e.stopPropagation()
    e.preventDefault()
    func(old => ({...old, current: modal}))
}